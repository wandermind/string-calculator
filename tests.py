import unittest

from calculator import Calculator


class CalculatorTestCase(unittest.TestCase):

    def setUp(self):
        self.calculator = Calculator("")

    def test_empty_string(self):
        sum = self.calculator.add()
        self.assertEqual(sum, 0)

    def test_single_number(self):
        self.calculator.input_string = "1"
        sum = self.calculator.add()
        self.assertEqual(sum, 1)

    def test_two_numbers(self):
        self.calculator.input_string = "1,2"
        sum = self.calculator.add()
        self.assertEqual(sum, 3)

    def test_string_with_multiple_numbers(self):
        self.calculator.input_string = "1,2,3,4,5,6,7"
        sum = self.calculator.add()
        self.assertEqual(sum, 28)

    def test_newline_character_as_delimiter(self):
        self.calculator.input_string = "1\n2,3"
        sum = self.calculator.add()
        self.assertEqual(sum, 6)

    def test_custom_delimiter(self):
        """Allow a custom symbol as delimiter

        Pattern: "//[delimiter]\n[numbers...]"
        """
        self.calculator.input_string = "//;\n1;2"
        sum = self.calculator.add()
        self.assertEqual(sum, 3)

    def test_reject_negative_numbers(self):
        self.calculator.input_string = "1,2,-3,4,-5"
        with self.assertRaises(ValueError) as context:
            self.calculator.add()
        self.assertTrue("-3,-5" in str(context.exception))

    def test_ignore_numbers_greater_than_1000(self):
        self.calculator.input_string = "1,1001"
        sum = self.calculator.add()
        self.assertEqual(sum, 1)

    def test_multiple_character_delimiter(self):
        self.calculator.input_string = "//[***]\n1***2***3"
        sum = self.calculator.add()
        self.assertEqual(sum, 6)

    def test_allow_multiple_delimiters(self):
        self.calculator.input_string = "//[*][%]\n1*2%3"
        sum = self.calculator.add()
        self.assertEqual(sum, 6)

    def test_allow_multiple_delimiters_of_multicharacter(self):
        self.calculator.input_string = "//[***][%%]\n1***2%%3***1"
        sum = self.calculator.add()
        self.assertEqual(sum, 7)
