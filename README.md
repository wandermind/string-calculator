### String calculator

An exercise to practice TDD using `python3` and `unittest` library.


##### Run test

To run unit test, execute `python3 -m unittest`


##### Run string calculator

- Open python console and try different cases

```
>>> from calculator import Calculator

# empty string
>>> calculator = Calculator("")
>>> calculator.add()
0

# multiple numbers with newline character as delimiter
>>> calculator.input_string = "1,2,3\n4,5"
>>> calculator.add()
15

# change delimiter to ';'
>>> calculator.input_string = "//;\n1;2;3\n4"
>>> calculator.add()
10

# with negative numbers
>>> calculator.input_string = "//;\n1;2;-3;-4"
>>> calculator.add()
ValueError: Negative numbers are not allowed: -3,-4

# ignore numbers > 1000
>>> calculator.input_string = "1,1001"
>>> calculator.add()
1

# multi-character delimiter
>>> calculator.input_string = "//[***]\n1***2***3"
>>> calculator.add()
6

# multiple delimiters
>>> calculator.input_string = "//[*][%]\n1*2%3"
>>> calculator.add()
6

# multiple multi-line delimiters
>>> calculator.input_string = "//[***][%%]\n1***2%%3***1"
>>> calculator.add()
7
```
