import re


class Calculator:
    """TDD Kata Exercise

    Add numbers from an input string

    Arg: a string which contains the numbers
    Return: sum of numbers as integer
    """

    def __init__(self, input_string):
        self.input_string = input_string
        self.delimiters = [',', '\n']

    def add(self):
        if self.input_string == "":
            return 0

        number_list = self.parse_numbers_from_input()
        negative_numbers_list = self.check_negative_numbers(number_list)

        if len(negative_numbers_list) > 0:
            negative_numbers = ','.join(negative_numbers_list)
            raise ValueError(
                f"Negative numbers are not allowed: {negative_numbers}"
            )

        sum = self.find_sum(number_list)
        return sum

    def find_sum(self, number_list):
        """Find sum of numbers (string) in a given list"""

        sum = 0
        for number in number_list:
            if int(number) <= 1000:
                sum += int(number)

        return sum

    def parse_numbers_from_input(self):
        """Parse the input string; check and update delimiter if required

        Return a list of string numbers
        """
        number_section = self.input_string

        if self.input_string.startswith('//'):
            delimiter_section, number_section = self.input_string.split('\n', 1)

            if delimiter_section.startswith('//['):
                # find patters like: [**], [***], [*][%] etc
                self.delimiters = re.findall(r'\[(.*?)\]', delimiter_section)
            else:
                self.delimiters = [delimiter_section[2]]

            # newline is a valid delimiter
            self.delimiters += ['\n']

        # convert multiple types of delimiters to single one(space) for easiness
        for delimiter in self.delimiters:
            number_section = number_section.replace(delimiter, ' ')

        number_list = number_section.split(' ')
        return number_list

    def check_negative_numbers(self, number_list):
        negative_numbers = filter(lambda number: int(number) < 0, number_list)
        return list(negative_numbers)
